from PyQt5.QtCore import QAbstractTableModel, QModelIndex, Qt


class TableModelRegitry(QAbstractTableModel):
    headers = ('App', 'Network', 'Sent', 'Recv')

    def __init__(self):
        super(TableModelRegitry, self).__init__()
        self.items = list()

    def data(self, index, role):
        if role == Qt.DisplayRole:
            item = self.items[index.row()]
            col = index.column()
            value = 'know'

            if col == 0:
                value = item.getApp()
            elif col == 1:
                value = item.device_name
            elif col == 2:
                value = item.getSent()
            elif col == 3:
                value = item.getRecv()
            return value

    def columnCount(self, index):
        return 4

    def headerData(self, section, orientation, role):
        # section is the index of the column/row.
        if role == Qt.DisplayRole:
            header = ''
            if orientation == Qt.Horizontal:
                header = self.headers[section]

            if orientation == Qt.Vertical:
                if len(self.items) > section:
                    item = self.items[section]
                    header = item.getId()

            return header

    def append(self, item):
        self.beginInsertRows(QModelIndex(),
                             self.rowCount(),
                             self.rowCount())

        self.items.append(item)
        self.endInsertRows()

    def clear(self):
        self.beginRemoveRows(QModelIndex(), 0, self.rowCount()-1)

        self.items = list()

        self.endRemoveRows()

    def getIndexById(self, rid):
        item = self.getItemById(rid)
        index = None
        if item:
            index = self.items.index(self.getItemById(rid))
        return index

    def getItemById(self, rid):
        return next((x for x in self.items if x.getId() == str(rid)), None)

    def getItemByApp(self, app):
        return next((x for x in self.items if x.getApp() == app), None)

    def remove(self, rid):
        position = self.getIndexById(rid)

        if position is not None:
            self.beginRemoveRows(QModelIndex(), position, position)

            item = self.getItemById(rid)
            if item:
                self.items.remove(item)

            self.endRemoveRows()


    def rowCount(self, parent=None):
        return len(self.items)
