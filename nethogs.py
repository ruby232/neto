from PyQt5.QtCore import QThread, pyqtSignal
from nethogs_misc import *
import ctypes
from regitry import Registry


class Nethogs(QThread):
    network_activity = pyqtSignal(Registry)
    loop_stop = pyqtSignal(str)
    
    def __init__(self, parent=None):
        super(Nethogs, self).__init__(parent)
        self.device_names = ['tun0', 'wlp6s0']
        self.LIBRARY_NAME = 'libnethogs.so.main'
        self.FILTER = 'not (src net (10 or 172.16/12 or 192.168/16)' \
                      ' and dst net (10 or 172.16/12 or 192.168/16))'
        self.lib = ctypes.CDLL(self.LIBRARY_NAME)

    def __del__(self):
        self.lib.nethogsmonitor_breakloop()
        del self.lib
        self.wait()

    def stop_monitor(self):
        self.lib.nethogsmonitor_breakloop()
        del self.lib
        self.lib = ctypes.CDLL(self.LIBRARY_NAME)
        self.run_monitor_loop()

    def run_monitor_loop(self):
        # Create a type for my callback func. The callback func returns void (None), and accepts as
        # params an int and a pointer to a NethogsMonitorRecord instance.
        # The params and return type of the callback function are mandated by nethogsmonitor_loop().
        # See libnethogs.h.
        CALLBACK_FUNC_TYPE = ctypes.CFUNCTYPE(
            ctypes.c_void_p, ctypes.c_int, ctypes.POINTER(NethogsMonitorRecord)
        )

        filter_arg = self.FILTER
        if filter_arg is not None:
            filter_arg = ctypes.c_char_p(filter_arg.encode('ascii'))

        if len(self.device_names) < 1:
            # monitor all devices
            rc = self.lib.nethogsmonitor_loop(
                CALLBACK_FUNC_TYPE(self.network_activity_callback),
                filter_arg
            )
        else:
            devc, devicenames = self.dev_args(self.device_names)
            rc = self.lib.nethogsmonitor_loop_devices(
                CALLBACK_FUNC_TYPE(self.network_activity_callback),
                filter_arg,
                devc,
                devicenames,
                ctypes.c_bool(True)
            )

        if rc != LoopStatus.OK:
            status = LoopStatus.MAP[rc]
            print('nethogsmonitor_loop returned {}'.format(status))
            self.loop_stop.emit(status)
           
        else:
            print('exiting monitor loop')

    def network_activity_callback(self, action, data):
        # Action type is either SET or REMOVE. I have never seen nethogs send an unknown action
        # type, and I don't expect it to do so.
        action_type = Action.MAP.get(action, 'Unknown')
        device_name = data.contents.device_name.decode('ascii')

        if device_name:
            registry = Registry(
                action_type,
                data.contents.record_id,
                str(data.contents.name),
                data.contents.pid,
                data.contents.uid,
                device_name,
                data.contents.sent_bytes,
                data.contents.recv_bytes)

            self.network_activity.emit(registry)

    def dev_args(self, devnames):
        """
        Return the appropriate ctypes arguments for a device name list, to pass
        to libnethogs ``nethogsmonitor_loop_devices``. The return value is a
        2-tuple of devc (``ctypes.c_int``) and devicenames (``ctypes.POINTER``)
        to an array of ``ctypes.c_char``).

        :param devnames: list of device names to monitor
        :type devnames: list
        :return: 2-tuple of devc, devicenames ctypes arguments
        :rtype: tuple
        """
        devc = len(devnames)
        devnames_type = ctypes.c_char_p * devc
        devnames_arg = devnames_type()
        for idx, val in enumerate(devnames):
            devnames_arg[idx] = (val + chr(0)).encode('ascii')
        return ctypes.c_int(devc), ctypes.cast(
            devnames_arg, ctypes.POINTER(ctypes.c_char_p)
        )

    def run(self):
        self.run_monitor_loop()
