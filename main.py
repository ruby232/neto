#!/usr/bin/python

import sys
from PyQt5.QtWidgets import QApplication, QWidget, QTableWidget, QTableWidgetItem, QHeaderView, QVBoxLayout, QTableView
from PyQt5.QtGui import QIcon
from nethogs import Nethogs
from network_status import NetworkStatus
from table_model_regitry_active import TableModelRegitryActive
from table_model_regitry_removed import TableModelRegitryRemoved


class Neto(QWidget):
    def __init__(self):
        super().__init__()
        self.initUI()

    def initUI(self):
        self.title = 'NetO'
        self.left = 0
        self.top = 0
        self.width = 600
        self.height = 600

        self.setWindowTitle(self.title)
        self.setWindowIcon(QIcon('icon.png'))
        self.setGeometry(self.left, self.top, self.width, self.height)

        self.createTables()
        self.init_nethogs()
        self.init_NetworkStatus()

        self.layout = QVBoxLayout()
        self.layout.addWidget(self.tv_active)
        self.layout.addWidget(self.tv_remove)
        self.setLayout(self.layout)
        self.show()


    def createTables(self):
        # List of active process
        self.tv_active = QTableView()
        self.modelActive = TableModelRegitryActive()
        self.tv_active.setModel(self.modelActive)

        # Table will fit the screen horizontally
        self.tv_active.horizontalHeader().setStretchLastSection(True)
        self.tv_active.horizontalHeader().setSectionResizeMode(QHeaderView.Stretch)

        # Table of removed process
        self.tv_remove = QTableView()
        self.modelRemoved = TableModelRegitryRemoved()
        self.tv_remove.setModel(self.modelRemoved)

        # Table will fit the screen horizontally
        self.tv_remove.horizontalHeader().setStretchLastSection(True)
        self.tv_remove.horizontalHeader().setSectionResizeMode(QHeaderView.Stretch)

    def init_nethogs(self):
        self.nethogs = Nethogs()
        self.nethogs.network_activity.connect(self.slotNetworkActivity)
        self.nethogs.loop_stop.connect(self.slotNethogsStop)
        self.nethogs.start()

    def init_NetworkStatus(self):
        self.networkStatus = NetworkStatus()
        self.networkStatus.network_change.connect(self.slotNetworkChange)
        self.networkStatus.start()

    def slotNethogsStop(self, status):
        self.networkRestart()
        
    def slotNetworkChange(self):
        self.networkRestart()
        
    def networkRestart(self):
        del self.nethogs
        for registry in self.modelActive.items:
            self.modelRemoved.append(registry)
        self.modelActive.clear()
        self.init_nethogs()


    def slotNetworkActivity(self, registry):
        if registry.isRemoved():
            self.modelRemoved.append(registry)
            self.modelActive.remove(registry.getId())
        else:
            self.modelActive.append(registry)

def main():
    app = QApplication(sys.argv)
    ex = Neto()
    sys.exit(app.exec_())


if __name__ == '__main__':
    main()
