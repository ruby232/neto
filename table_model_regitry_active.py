from table_model_regitry import TableModelRegitry

class TableModelRegitryActive(TableModelRegitry):
    def __init__(self):
        super(TableModelRegitryActive, self).__init__()
        self.items = list()

    def append(self, item):
        _item = self.getItemById(item.getId())
        if _item:
            index = self.items.index(_item)
            _item.updated(item.action_type, item.sent, item.recv)
            self.items[index] = _item
        else:
            super().append(item)




