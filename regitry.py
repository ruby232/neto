import bitmath
import psutil
from datetime import datetime


class Registry:
    def __init__(self, action_type, record_id, name, pid, uid, device_name, sent, recv):
        self.id = record_id
        self.action_type = action_type
        self.name = name
        self.pid = pid
        self.uid = uid
        self.device_name = device_name
        self.sent = sent
        self.recv = recv
        self.start = datetime.now()
        self.finish = None
        
        try:
             process = psutil.Process(self.pid)
             self.app = process.name()
        except:
            self.app = name

    def getId(self):
        return str(self.id)

    def updated(self, action_type, sent, recv):
        self.action_type = action_type
        self.sent = sent
        self.recv = recv
        if action_type == 'REMOVE':
            self.finish = datetime.now()

    def getApp(self):        
        return self.app

    def getSent(self):
        return self.bytes_2_human_readable(self.sent)

    def getRecv(self):
        return self.bytes_2_human_readable(self.recv)

    def bytes_2_human_readable(self, bytes):
        bitmath.format_string = '{value:0.2f} {unit}'
        size = bitmath.Byte(bytes)
        return str(size.best_prefix())

    def isRemoved(self):
        return self.action_type == 'REMOVE'

    def __str__(self):
        return 'ID: {}, App:{}'.format(self.id, self.getApp())

