# Compile libnethogs.so.main
1. Download https://github.com/raboof/nethogs
2. Build it with `make libnethogs`, install with `make install_lib` or `make install_dev`.

# Install dependencies
```sh
pip3 install psutil bitmath
