from PyQt5.QtCore import QThread, pyqtSignal
import psutil
import time


class NetworkStatus(QThread):
    network_change = pyqtSignal()

    def __init__(self, parent=None):
        super(NetworkStatus, self).__init__(parent)
        self.working = True
        self.inVPN = None

    def __del__(self):
        self.working = False
        self.wait()

    def monitor_network(self):
        addresses = psutil.net_if_addrs()
        stats = psutil.net_if_stats()
        _inVPN = False

        for intface, addr_list in addresses.items():
            if any(getattr(addr, 'address').startswith("169.254") for addr in addr_list):
                continue
            elif intface in stats and getattr(stats[intface], "isup"):
                if intface == 'tun0':
                    _inVPN = True
                    break

        if self.inVPN is not None and _inVPN != self.inVPN:
            self.network_change.emit()
        self.inVPN = _inVPN

    def run(self):
        while self.working == True:
            self.monitor_network()
            time.sleep(2)
