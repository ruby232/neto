from table_model_regitry import TableModelRegitry

class TableModelRegitryRemoved(TableModelRegitry):
    def __init__(self):
        super(TableModelRegitryRemoved, self).__init__()
        self.items = list()

    def append(self, item):
        _item = self.getItemByApp(item.getApp())
        if _item:
            index = self.items.index(_item)
            _item.action_type = _item.action_type
            _item.sent = _item.sent + item.sent
            _item.recv = _item.recv + item.recv
            self.items[index] = _item
        else:
            super().append(item)




